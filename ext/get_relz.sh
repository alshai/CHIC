#!/usr/bin/env bash
set -o errexit
set -o nounset

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

TARGET_FOLDER="${DIR}/ReLZ"
if [ ! -d "${TARGET_FOLDER}" ]; then

  echo "*********************************"
  echo "Compiling and installing ReLZ"
  echo "*********************************"
  
  COMMIT="master"
  git clone https://gitlab.com/dvalenzu/ReLZ.git
	cd ReLZ;
  git checkout ${COMMIT}
	make;
	cd ..;
else
  echo "ReLZ ALREADY INSTALLED"
fi

